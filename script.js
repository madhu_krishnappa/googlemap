angular.module('appMaps', ['uiGmapgoogle-maps'])
    .config(function(uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          //    key: 'your api key',
          v: '3.17',
          libraries: 'places' // Required for SearchBox.
      });
    })
    .controller('mainCtrl', function ($scope, $log) {
        $scope.map = {center: {latitude: 21.1451, longitude: 78.6680 }, zoom: 4 ,
            
            events:{
                         click: function (map, eventName, originalEventArgs) 
                       {
                         var e = originalEventArgs[0];
                         var lat = e.latLng.lat(),lon = e.latLng.lng();
                         var marker = {
                                       id: Date.now(),
                                    coords: {
                                    latitude: lat,
                                   longitude: lon,

                                            },
                                      massage :"marker"
                          };
                          $scope.markers.push(marker);
                          alert("markers" +JSON.stringify($scope.markers) );
                          console.log($scope.markers);
                            $scope.$apply();

                       }
            }
                     };
        $scope.options = {scrollwheel: false};
         
        $scope.markers = [];
    
        var events = {
          places_changed: function (searchBox) {var place = searchBox.getPlaces();
        if (!place || place == 'undefined' || place.length == 0) {
            console.log('no place data :(');
            return;
        }

        $scope.map = {
            center: {
                latitude: place[0].geometry.location.lat(),
                longitude: place[0].geometry.location.lng()
            },
            zoom: 13,
        
            events:{
                         click: function (map, eventName, originalEventArgs) 
                       {
                         var e = originalEventArgs[0];
                         var lat = e.latLng.lat(),lon = e.latLng.lng();
                         var marker = {
                                       id: Date.now(),
                                    coords: {
                                    latitude: lat,
                                   longitude: lon,

                                            },
                                      massage :"marker"
                          };
                          $scope.markers.push(marker);
                          alert("markers" +JSON.stringify($scope.markers) );
                          console.log($scope.markers);
                            $scope.$apply();

                       }
            }
        };
            
          }
        }
        $scope.searchbox = { template:'searchbox.tpl.html', events:events};
   
    });

